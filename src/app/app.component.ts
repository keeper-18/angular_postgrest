import { Component, OnInit } from '@angular/core';
import { HeroesService } from './heroes.service';
import { Hero } from './hero';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  heroes: Array<Hero>;
  hero: Hero;
  heroName = '';
  selectedHero: Hero;

  constructor(private heroesService: HeroesService) { }

  ngOnInit() {
    this.heroesService.getHeroes()
      .subscribe(
        data => this.heroes = data
    );
  }

  refreshListOfHeroes() {
    this.heroesService.getHeroes()
      .subscribe(
        data => this.heroes = data
      );
  }

  onGetHero() {
    if (this.selectedHero) {
      this.heroesService.getHero(this.selectedHero)
        .subscribe(
          data => this.hero = data,
          error => console.error(error)
        );
    }
  }

  onSelectedHero(hero: Hero) {
    if (hero) {
      this.selectedHero = hero;
      this.heroName = hero.name;
      console.log('you have picked a hero');
    }
  }

  onAddHero(heroName: Hero) {
    if (heroName) {
      this.heroesService.addHero(heroName)
        .subscribe(
          () => {
            this.refreshListOfHeroes();
            this.heroName = '';
          }
    );
    } else {
      console.log('add hero name');
    }
  }

  onUpdateHero() {
    if (this.selectedHero) {
      this.selectedHero.name = this.heroName ? this.heroName : this.selectedHero.name;
      this.heroesService.updateHero(this.selectedHero)
        .subscribe(
          () => {
            this.refreshListOfHeroes();
            this.heroName = '';
          }
        );
    } else {
      console.log('pick the hero');
    }
  }

  onDeleteHero () {
    if (this.selectedHero) {
      this.heroesService.deleteHero(this.selectedHero)
        .subscribe(
          () => {
            this.refreshListOfHeroes();
            this.heroName = '';
          }
        );
    } else {
      console.log('pick the hero');
    }
  }
}

