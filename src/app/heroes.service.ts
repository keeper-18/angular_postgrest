import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Hero } from './hero';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class HeroesService {

  public url = 'http://localhost:3000/heroes';
  public httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  getHeroes(): Observable<Hero[]> {
    return this.http.get<Hero[]>(this.url);
  }

  getHero(hero: Hero): Observable<Hero> {
    return this.http.get<Hero>(this.url + '?id=eq.' + hero.id);
  }

  addHero(heroName: Hero): Observable<Hero> {
    const hero = {
      name: heroName
    };
    return this.http.post<Hero>(this.url, hero, this.httpOptions);
  }

  updateHero (hero: Hero) {
      return this.http.patch(this.url + '?id=eq.' + hero.id, hero, this.httpOptions);
  }

  deleteHero (hero: Hero): Observable<Object> {
    return this.http.delete<Object>(this.url + '?id=eq.' + hero.id, this.httpOptions);
  }

}
